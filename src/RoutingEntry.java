/**
 * This class contains all information about each route in the routing table.
 *
 * @author Justin Ervin (Group 2)
 * @version 1-21-2015
 */

import java.util.regex.Pattern;

public class RoutingEntry {
	private StringBuilder networkAddress = new StringBuilder();
	private int[] routeIPAddress = null;
	private String subnetMask = null;
	private String interfaceName = null;
	private int subnetNumber;

	/**
	 * Constructor for objects of class Date
	 */
	public RoutingEntry(String addressMask, String interfaceName) {
		this.routeIPAddress = new int[4];
		this.interfaceName = interfaceName;

		if (addressMask != null) {
			String[] subnetData = addressMask.split(Pattern.quote("/"));

			if (subnetData.length > 1) {
				// Creates the binary format of the subnet mask from the subnet number.
				this.subnetMask = calculateSubnetBinaryMask(Integer.parseInt(subnetData[1]));

				if (isIPAddress(subnetData[0])) {
					String[] ipData = subnetData[0].split(Pattern.quote("."));

					// Convert the IP Address from String to integer array
					for (int index = 0; index < ipData.length; index++) {
						if (ipData[index] != null) {
							routeIPAddress[index] = Integer.parseInt(ipData[index].trim());
						}
					}

					this.subnetNumber = Integer.parseInt(subnetData[1]);

					// Converting the route IP Address to binary form for generating the network
					// part
					String routeIP = convertDecimalToBinary(routeIPAddress);

					// Generate the network part by doing binary AND of the subnet mask with the
					// input IP address
					for (int index = 0; index < 32; index++) {
						if (routeIP.charAt(index) == '1' && subnetMask.charAt(index) == '1') {
							networkAddress.append(1);
						} else if (subnetMask.charAt(index) != '0') {
							networkAddress.append(0);
						}
					}
				}
			}
		}
	}

	/**
	 * Creates the binary format of the subnet mask from the subnet number.
	 */
	public String calculateSubnetBinaryMask(int subnet) {
		StringBuilder mask = new StringBuilder();

		for (int index = 0; index < 32; index++) {
			if (subnet > index) {
				mask.append(1);
			} else {
				mask.append(0);
			}
		}

		return mask.toString();
	}

	/**
	 * Check whether or not the input IP Address matches the network part of this route
	 */
	public boolean compare(int[] ipAddress) {
		// Converting the input IP Address to binary form
		String binaryIP = convertDecimalToBinary(ipAddress);

		if (subnetMask == null) {
			return false;
		}

		// Comparing the binary of the input IP Address with the binary of the network part
		for (int index = 0; index < networkAddress.length(); index++) {
			if (binaryIP.charAt(index) != networkAddress.charAt(index)) {
				return false;
			}
		}

		return true;
	}

	/**
	 * Used to compare two routes for longest prefix match
	 */
	public int compareTo(RoutingEntry other) {
		if (this.subnetNumber > other.subnetNumber) {
			return 1;
		} else if (this.subnetNumber < other.subnetNumber) {
			return -1;
		} else {
			return 0;
		}
	}

	/**
	 * Converts a IP Address from decimal form to binary form
	 */
	public String convertDecimalToBinary(int[] decimal) {
		StringBuilder binary = new StringBuilder();
		int remains;

		for (int octet = 0; octet < decimal.length; octet++) {
			remains = decimal[octet];

			for (int index = 7; index >= 0; index--) {
				if (remains - Math.pow(2, index) >= 0) {
					remains -= Math.pow(2, index);
					binary.append(1);
				} else {
					binary.append(0);
				}
			}
		}

		return binary.toString();
	}

	/**
	 * Returns the Interface`s name
	 */
	public String getInterfaceName() {
		return interfaceName;
	}

	/**
	 * Returns the route IP Address for the route in integer array
	 */
	public int[] getRouteIPAddress() {
		return routeIPAddress;
	}

	/**
	 * Returns the subnet mask for the route in binary form
	 */
	public String getSubnetMask() {
		return subnetMask;
	}

	/**
	 * Returns the route IP Address and the subnet mask number for the route
	 */
	public String getIPAddressAndMask() {
		return routeIPAddress[0] + "." + routeIPAddress[1] + "." + routeIPAddress[2] + "." + routeIPAddress[3] + "/" + subnetNumber;
	}

	/**
	 * Checks whether or not this is the default route
	 */
	public boolean isDefault() {
		if (routeIPAddress[0] == 0 && routeIPAddress[1] == 0 && routeIPAddress[2] == 0 && routeIPAddress[3] == 0) {
			return true;
		}

		return false;
	}

	/**
	 * Checks whether or not the input IP Address is the right syntax
	 */
	public static boolean isIPAddress(final String input) {
		return Pattern.compile("^(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)(\\.(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)){3}$").matcher(input).matches();
	}

	/**
	 * Checks whether or not the input subnet mask number is in the correct range
	 */
	public static boolean isSubsetNumber(final String input) {
		return Pattern.compile("^(3[0-2]|[0-2]?\\d)$").matcher(input).matches();
	}

	@Override
	public String toString() {
		if (isDefault()) {
			return "default > " + interfaceName;
		} else {
			return routeIPAddress[0] + "." + routeIPAddress[1] + "." + routeIPAddress[2] + "." + routeIPAddress[3] + "/" + subnetNumber + " > " + interfaceName;
		}
	}
}
