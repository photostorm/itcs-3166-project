/**
 * This class is used to run simulation showing which packet gets routed to which Interface at any given time.
 *
 * @author Justin Ervin (Group 2)
 * @version 1-21-2015
 */

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.regex.Pattern;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;

public class RouterMain {
	private class ClickListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == btnRoute) {
				if (isDefaultAdded) {
					// Checks whether or not the input IP Address is the right syntax
					if (RoutingEntry.isIPAddress(txtRouteIPAddress.getText())) {
						String[] ipData = txtRouteIPAddress.getText().split(Pattern.quote("."));
						int[] address = new int[4];

						// Convert the IP Address from String to integer array
						for (int index = 0; index < ipData.length; index++) {
							if (ipData[index] != null) {
								address[index] = Integer.parseInt(ipData[index].trim());
							}
						}

						// Find the route
						RoutingEntry result = findRoute(address, routingTable);

						// Print the result to the output textbox
						if (result.isDefault()) {
							txtOutput.setText("A packet from " + txtRouteIPAddress.getText() + " is routed to " + result.getInterfaceName() + " at the default gateway.");
						} else {
							txtOutput.setText("A packet from " + txtRouteIPAddress.getText() + " is routed to " + result.getInterfaceName() + " at " + result.getIPAddressAndMask() + ".");
						}
					} else {
						JOptionPane.showMessageDialog(null, "Invalid IP Address");
					}
				} else {
					JOptionPane.showMessageDialog(null, "No default route added");
				}
			} else if (e.getSource() == btnDeleteRoute) {
				int selectedValue = lstRouteTable.getSelectedIndex();

				if (selectedValue != -1) {
					if (listModel.get(selectedValue) != null) {
						boolean isFound = false;

						// Searching for the route in the routing table, so we can remove it.
						for (int i = 0; i < routingTable.size() && !isFound; i++) {
							if (routingTable.get(i) != null) {
								if (listModel.get(selectedValue).equalsIgnoreCase(routingTable.get(i).toString())) {
									if (routingTable.get(i).isDefault()) {
										isDefaultAdded = false;
									}

									routingTable.remove(i);
									isFound = true;
								}
							}
						}
					}

					listModel.remove(selectedValue);
				} else {
					JOptionPane.showMessageDialog(null, "Nothing was Selected");
				}
			} else if (e.getSource() == btnAddRoute) {
				String[] maskData = txtTableIPAddress.getText().split(Pattern.quote("/"));

				// Validity that the route IP Address, subnet number, and the interface name is
				// valid
				if (txtTableIPAddress.getText().length() > 0 && txtInterfaceName.getText().length() > 0) {
					if (maskData.length > 1) {
						if (!listModel.contains(txtTableIPAddress.getText() + " > " + txtInterfaceName.getText()) && RoutingEntry.isIPAddress(maskData[0]) && RoutingEntry.isSubsetNumber(maskData[1])) {
							addRoute(txtTableIPAddress.getText(), txtInterfaceName.getText());
						} else {
							JOptionPane.showMessageDialog(null, "Already in the Routing Table");
						}
					} else if (txtTableIPAddress.getText().trim().equalsIgnoreCase("default")) {
						if (!isDefaultAdded) {
							addRoute("default", txtInterfaceName.getText());
						} else {
							JOptionPane.showMessageDialog(null, "Already in the Routing Table");
						}
					} else {
						JOptionPane.showMessageDialog(null, "Invalid IP Address/Mask");
					}
				} else {
					JOptionPane.showMessageDialog(null, "Invalid Information");
				}
			}
		}
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					RouterMain window = new RouterMain();
					window.frmNetwork.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	// Routing Table
	private static ArrayList<RoutingEntry> routingTable = new ArrayList<RoutingEntry>();
	private boolean isDefaultAdded;

	// All the components for the Graphical User Interface
	private JFrame frmNetwork;
	private JTextField txtTableIPAddress;
	private JTextField txtInterfaceName;
	private JTextField txtRouteIPAddress;
	private JList<String> lstRouteTable;
	private JTextArea txtOutput;
	private DefaultListModel<String> listModel;
	private JButton btnAddRoute;
	private JButton btnDeleteRoute;
	private JButton btnRoute;

	/**
	 * Create the application.
	 */
	public RouterMain() {
		initialize();

		// Use the data from Exercise 33 in Chapter 5
		addRoute("135.46.56.0/22", "Interface 0");
		addRoute("135.46.60.0/22", "Interface 1");
		addRoute("192.53.40.0/23", "Router 1");
		addRoute("default", "Router 2");
	}

	/**
	 * Add a route to the routing table.
	 */
	public void addRoute(String addressMask, String interfaceName) {
		// Adding the route to the listbox and the routing table array list
		listModel.addElement(addressMask + " > " + interfaceName);
		routingTable.add(new RoutingEntry(addressMask, interfaceName));

		// Check if the new route is the default route
		if (!isDefaultAdded && addressMask.equalsIgnoreCase("default")) {
			isDefaultAdded = true;
		}

		// Sort the routing table for longest prefix match
		sortTable(routingTable);
	}

	/**
	 * Find a route in the routing table.
	 */
	public RoutingEntry findRoute(int[] address, ArrayList<RoutingEntry> routingTable) {
		RoutingEntry interfaceName = null;
		RoutingEntry defaultInterface = null;

		// Searching through the route table
		for (int index = 0; index < routingTable.size() && interfaceName == null; index++) {
			if (routingTable.get(index) != null) {
				if (routingTable.get(index).isDefault()) {
					defaultInterface = routingTable.get(index);
				} else if (routingTable.get(index).compare(address)) {
					interfaceName = routingTable.get(index);
				}
			}
		}

		// Return the default interface if the IP address does not match with one of the interfaces
		if (interfaceName == null) {
			return defaultInterface;
		}

		// If the IP address matches with one of interface , it will return the matching Interface
		return interfaceName;
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		// Creating the click listener for all the button
		ClickListener cl = new ClickListener();

		// Creating the window frame
		frmNetwork = new JFrame();
		frmNetwork.setResizable(false);
		frmNetwork.setTitle("ITCS 3166 Project (Group 2) - Routing Implementation");
		frmNetwork.setBounds(100, 100, 710, 521);
		frmNetwork.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmNetwork.getContentPane().setLayout(null);

		// Creating the model for the routing table listbox
		listModel = new DefaultListModel<String>();
		lstRouteTable = new JList<String>(listModel);

		// Creating the panel that contains all fields for the route table
		JPanel routingTablePanel = new JPanel();
		routingTablePanel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Input Routing Table", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		routingTablePanel.setBounds(310, 32, 370, 173);
		frmNetwork.getContentPane().add(routingTablePanel);
		routingTablePanel.setLayout(null);

		// Creating the label for the routing IP address and subset mask textbox
		JLabel lblTableIPAddress = new JLabel("IP Address/Mask (Example: 135.46.56.0/22)");
		lblTableIPAddress.setBounds(12, 25, 260, 16);
		routingTablePanel.add(lblTableIPAddress);

		// Creating the textbox for the routing IP address and subset mask
		txtTableIPAddress = new JTextField();
		txtTableIPAddress.setBounds(12, 42, 347, 22);
		routingTablePanel.add(txtTableIPAddress);
		txtTableIPAddress.setColumns(10);

		// Creating the label for the interface name textbox
		JLabel lblInterfaceName = new JLabel("Interface Name");
		lblInterfaceName.setBounds(12, 77, 123, 16);
		routingTablePanel.add(lblInterfaceName);

		// Creating the textbox for the interface name
		txtInterfaceName = new JTextField();
		txtInterfaceName.setBounds(12, 94, 347, 22);
		routingTablePanel.add(txtInterfaceName);
		txtInterfaceName.setColumns(10);

		// Creating the button for adding routes
		btnAddRoute = new JButton("Add Route");
		btnAddRoute.setForeground(Color.WHITE);
		btnAddRoute.setBounds(202, 129, 157, 25);
		btnAddRoute.setBackground(new Color(66, 139, 202));
		btnAddRoute.addActionListener(cl);
		routingTablePanel.add(btnAddRoute);

		// Creating the panel for the input route IP Address
		JPanel routePanel = new JPanel();
		routePanel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Input Route", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		routePanel.setBounds(310, 218, 370, 115);
		frmNetwork.getContentPane().add(routePanel);
		routePanel.setLayout(null);

		// Creating the label for the input route IP Address
		JLabel lblRouteIPAddress = new JLabel("IP Address");
		lblRouteIPAddress.setBounds(12, 25, 83, 16);
		routePanel.add(lblRouteIPAddress);

		// Creating the textbox for the input route IP Address
		txtRouteIPAddress = new JTextField();
		txtRouteIPAddress.setBounds(12, 42, 347, 22);
		routePanel.add(txtRouteIPAddress);
		txtRouteIPAddress.setColumns(10);

		// Creating the button for looking up routes
		btnRoute = new JButton("Lookup Route");
		btnRoute.setForeground(Color.WHITE);
		btnRoute.setBounds(202, 77, 157, 25);
		btnRoute.setBackground(new Color(66, 139, 202));
		btnRoute.addActionListener(cl);
		routePanel.add(btnRoute);

		// Creating the label for the routing table listbox
		JLabel lblRoutingTable = new JLabel("Routing Table");
		lblRoutingTable.setBounds(12, 13, 104, 16);
		frmNetwork.getContentPane().add(lblRoutingTable);

		// Creating the view for the output of the lookup
		txtOutput = new JTextArea("Instructions:\n1. Fill in values for the Routing Table Addresses. For the default value enter 'default' in IP Address/Mask textbox.\n" + "2. Enter the IP Address to lookup and click the Lookup Route button to run the program.");
		txtOutput.setEditable(false);
		txtOutput.setBounds(12, 346, 668, 115);
		frmNetwork.getContentPane().add(txtOutput);

		// Creating the button for deleting routes
		btnDeleteRoute = new JButton("Delete Route");
		btnDeleteRoute.setBounds(12, 308, 266, 25);
		btnDeleteRoute.setBackground(new Color(66, 139, 202));
		btnDeleteRoute.setForeground(Color.WHITE);
		btnDeleteRoute.addActionListener(cl);
		frmNetwork.getContentPane().add(btnDeleteRoute);

		// Allowing the routing list to scroll when too many routes are added
		JScrollPane routeScrollPane = new JScrollPane(lstRouteTable);
		routeScrollPane.setBounds(12, 32, 266, 270);
		frmNetwork.getContentPane().add(routeScrollPane);
	}

	/**
	 * Sort the routing table for longest prefix match
	 */
	private void sortTable(ArrayList<RoutingEntry> routingTable) {
		int startScan; // Holds the start of the current range
		int arrayIndex; // Holds the current index of array being read through
		int maxIndex;
		RoutingEntry maxValue;

		for (startScan = 0; startScan < (routingTable.size() - 1); startScan++) {
			// Set the index and value of the start value of the current range
			maxIndex = startScan;
			maxValue = routingTable.get(startScan);

			for (arrayIndex = startScan + 1; arrayIndex < routingTable.size(); arrayIndex++) {
				if (routingTable.get(arrayIndex).compareTo(maxValue) > 0) {
					maxValue = routingTable.get(arrayIndex);
					maxIndex = arrayIndex;
				}
			}

			// Setting the current max value to it new position
			routingTable.set(maxIndex, routingTable.get(startScan));
			routingTable.set(startScan, maxValue);
		}
	}
}